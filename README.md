# README #

> "*The [`IOTCODES.ORG`](http://www.iotcodes.org) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.*"

Therefore [`IOTCODES.ORG`](http://www.iotcodes.org) ([`org.iotcodes`](https://bitbucket.org/iotcodes) group) group of artifacts is published under some open source licenses; covered by this artifact:

Each artifact available under the below licensing agreements includes this [`iotcodes-licensing`](https://bitbucket.org/iotcodes/iotcodes-licensing) ([`org.iotcodes`](https://bitbucket.org/iotcodes) group) artifact in its maven (gradle) artifact dependency - for example in the artifact's `pom.xml`.

## What is this repository for? ##

The [`iotcodes-licensing`](https://bitbucket.org/iotcodes/iotcodes-licensing)  artifact is a meta-artifact included as a dependency into all artifacts which apply the herein contained licensing terms (usually artifacts of the group [`org.iotcodes`](https://bitbucket.org/iotcodes)). Them  [`iotcodes-licensing`](https://bitbucket.org/iotcodes/iotcodes-licensing) terms and conditions can be summarized as below. Please see the [`iotcodes-licensing`](https://bitbucket.org/iotcodes/iotcodes-licensing) artifact of the version being applied to the artifact in question for the terms and conditions effectively being applied.

## IOTCODES.ORG ##

Below find the most current [`IOTCODES.ORG`](http://www.iotcodes.org) terms and conditions as of the time of this writing (2015-01-27). 

### Licensing terms and conditions ###

    /////////////////////////////////////////////////////////////////////////////
    IOTCODES.ORG
    =============================================================================
    This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
    under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
    licenses:
    =============================================================================
    GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
    together with the GPL linking exception applied; as applied by the GNU Classpath
    ("http://www.gnu.org/software/classpath/license.html")
    =============================================================================
    Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
    =============================================================================
    Please contact the copyright holding author(s) of the software artifacts in
    question for licensing issues not being covered by the above listed licenses,
    also regarding commercial licensing models or regarding the compatibility
    with other open source licenses.
    /////////////////////////////////////////////////////////////////////////////

## Who do I talk to? ##

* Siegfried Steiner (steiner@iotcodes.org)

## Terms and conditions ##

The [`IOTCODES.ORG`](http://www.iotcodes.org) group of artifacts is published under some open source licenses; covered by the  [`iotcodes-licensing`](https://bitbucket.org/iotcodes/iotcodes-licensing) ([`org.iotcodes`](https://bitbucket.org/iotcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
